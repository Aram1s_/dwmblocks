//Modify this file to change what commands output to your statusbar, and recompile using the make command.
static const Block blocks[] = {
	/*Icon*/	/*Command*/		/*Update Interval*/	/*Update Signal*/
	{" Cpu:" ,"mpstat | awk 'END{printf \"%.1f%\", 100 - $13}'",												  5,		0},
	{"Gpu:","nvidia-settings -q gpucoretemp -t | awk 'NR==1{printf \"%s°C\", $1}'",			5,		1},
	{"Sound:", "amixer get Master | awk -F'[][]' '/Left:/ { print $2 }'",								5,		2},
	{"Mem:", "free -h | awk '/^Mem/ { print $3\"/\"$2 }' | sed s/i//g",									30,		3},
	{"", "date '+%F %R:%S'",																														1,		4},
};

//sets delimeter between status commands. NULL character ('\0') means no delimeter.
static char delim[] = " | ";
static unsigned int delimLen = 5;
